
# Io.Gitlab.Mobwa.Model.CreateUserRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Username** | **string** |  | [optional] 
**Email** | **string** |  | [optional] 
**Password** | **string** |  | [optional] 
**Role** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models)
[[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to README]](../README.md)

