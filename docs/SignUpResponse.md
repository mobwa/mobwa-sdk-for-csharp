
# Io.Gitlab.Mobwa.Model.SignUpResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Username** | **string** |  | [optional] 
**Email** | **string** |  | [optional] 
**Role** | **string** |  | [optional] 
**Accounts** | [**List&lt;Account&gt;**](Account.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models)
[[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to README]](../README.md)

