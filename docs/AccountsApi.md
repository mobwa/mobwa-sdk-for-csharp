# Io.Gitlab.Mobwa.Api.AccountsApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CreateAccount**](AccountsApi.md#createaccount) | **POST** /accounts | Create an account
[**GetAccount**](AccountsApi.md#getaccount) | **GET** /accounts/{accountId} | Get account information
[**ListAccount**](AccountsApi.md#listaccount) | **GET** /accounts | List all accounts
[**RechargeAccount**](AccountsApi.md#rechargeaccount) | **POST** /accounts/{accountId}/recharge | Recharge the account
[**UpdateAccount**](AccountsApi.md#updateaccount) | **PUT** /accounts/{accountId} | Update account information
[**WithdrawMoney**](AccountsApi.md#withdrawmoney) | **POST** /accounts/{accountId}/withdraw | Withdraw money from the account



## CreateAccount

> List&lt;Transfer&gt; CreateAccount (CreateAccountRequest createAccountRequest = null)

Create an account

### Example

```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Io.Gitlab.Mobwa.Api;
using Io.Gitlab.Mobwa.Client;
using Io.Gitlab.Mobwa.Model;

namespace Example
{
    public class CreateAccountExample
    {
        public static void Main()
        {
            Configuration.Default.BasePath = "http://localhost";
            // Configure HTTP basic authorization: basicAuth
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";

            var apiInstance = new AccountsApi(Configuration.Default);
            var createAccountRequest = new CreateAccountRequest(); // CreateAccountRequest |  (optional) 

            try
            {
                // Create an account
                List<Transfer> result = apiInstance.CreateAccount(createAccountRequest);
                Debug.WriteLine(result);
            }
            catch (ApiException e)
            {
                Debug.Print("Exception when calling AccountsApi.CreateAccount: " + e.Message );
                Debug.Print("Status Code: "+ e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **createAccountRequest** | [**CreateAccountRequest**](CreateAccountRequest.md)|  | [optional] 

### Return type

[**List&lt;Transfer&gt;**](Transfer.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | A paged array of pets |  -  |
| **0** | unexpected error |  -  |

[[Back to top]](#)
[[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetAccount

> Account GetAccount (string accountId)

Get account information

### Example

```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Io.Gitlab.Mobwa.Api;
using Io.Gitlab.Mobwa.Client;
using Io.Gitlab.Mobwa.Model;

namespace Example
{
    public class GetAccountExample
    {
        public static void Main()
        {
            Configuration.Default.BasePath = "http://localhost";
            // Configure HTTP basic authorization: basicAuth
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";

            var apiInstance = new AccountsApi(Configuration.Default);
            var accountId = accountId_example;  // string | The id of the account

            try
            {
                // Get account information
                Account result = apiInstance.GetAccount(accountId);
                Debug.WriteLine(result);
            }
            catch (ApiException e)
            {
                Debug.Print("Exception when calling AccountsApi.GetAccount: " + e.Message );
                Debug.Print("Status Code: "+ e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accountId** | **string**| The id of the account | 

### Return type

[**Account**](Account.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Expected response to a valid request |  -  |
| **0** | unexpected error |  -  |

[[Back to top]](#)
[[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ListAccount

> List&lt;Account&gt; ListAccount ()

List all accounts

### Example

```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Io.Gitlab.Mobwa.Api;
using Io.Gitlab.Mobwa.Client;
using Io.Gitlab.Mobwa.Model;

namespace Example
{
    public class ListAccountExample
    {
        public static void Main()
        {
            Configuration.Default.BasePath = "http://localhost";
            // Configure HTTP basic authorization: basicAuth
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";

            var apiInstance = new AccountsApi(Configuration.Default);

            try
            {
                // List all accounts
                List<Account> result = apiInstance.ListAccount();
                Debug.WriteLine(result);
            }
            catch (ApiException e)
            {
                Debug.Print("Exception when calling AccountsApi.ListAccount: " + e.Message );
                Debug.Print("Status Code: "+ e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**List&lt;Account&gt;**](Account.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | A paged array of pets |  -  |
| **0** | unexpected error |  -  |

[[Back to top]](#)
[[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## RechargeAccount

> Account RechargeAccount (string accountId, RechargeAccountRequest rechargeAccountRequest = null)

Recharge the account

### Example

```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Io.Gitlab.Mobwa.Api;
using Io.Gitlab.Mobwa.Client;
using Io.Gitlab.Mobwa.Model;

namespace Example
{
    public class RechargeAccountExample
    {
        public static void Main()
        {
            Configuration.Default.BasePath = "http://localhost";
            // Configure HTTP basic authorization: basicAuth
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";

            var apiInstance = new AccountsApi(Configuration.Default);
            var accountId = accountId_example;  // string | The id of the account
            var rechargeAccountRequest = new RechargeAccountRequest(); // RechargeAccountRequest |  (optional) 

            try
            {
                // Recharge the account
                Account result = apiInstance.RechargeAccount(accountId, rechargeAccountRequest);
                Debug.WriteLine(result);
            }
            catch (ApiException e)
            {
                Debug.Print("Exception when calling AccountsApi.RechargeAccount: " + e.Message );
                Debug.Print("Status Code: "+ e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accountId** | **string**| The id of the account | 
 **rechargeAccountRequest** | [**RechargeAccountRequest**](RechargeAccountRequest.md)|  | [optional] 

### Return type

[**Account**](Account.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Expected response to a valid request |  -  |
| **0** | unexpected error |  -  |

[[Back to top]](#)
[[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UpdateAccount

> Account UpdateAccount (string accountId, UpdateAccountRequest updateAccountRequest = null)

Update account information

### Example

```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Io.Gitlab.Mobwa.Api;
using Io.Gitlab.Mobwa.Client;
using Io.Gitlab.Mobwa.Model;

namespace Example
{
    public class UpdateAccountExample
    {
        public static void Main()
        {
            Configuration.Default.BasePath = "http://localhost";
            // Configure HTTP basic authorization: basicAuth
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";

            var apiInstance = new AccountsApi(Configuration.Default);
            var accountId = accountId_example;  // string | The id of the account
            var updateAccountRequest = new UpdateAccountRequest(); // UpdateAccountRequest |  (optional) 

            try
            {
                // Update account information
                Account result = apiInstance.UpdateAccount(accountId, updateAccountRequest);
                Debug.WriteLine(result);
            }
            catch (ApiException e)
            {
                Debug.Print("Exception when calling AccountsApi.UpdateAccount: " + e.Message );
                Debug.Print("Status Code: "+ e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accountId** | **string**| The id of the account | 
 **updateAccountRequest** | [**UpdateAccountRequest**](UpdateAccountRequest.md)|  | [optional] 

### Return type

[**Account**](Account.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Expected response to a valid request |  -  |
| **0** | unexpected error |  -  |

[[Back to top]](#)
[[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## WithdrawMoney

> Account WithdrawMoney (string accountId, RechargeAccountRequest body = null)

Withdraw money from the account

### Example

```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Io.Gitlab.Mobwa.Api;
using Io.Gitlab.Mobwa.Client;
using Io.Gitlab.Mobwa.Model;

namespace Example
{
    public class WithdrawMoneyExample
    {
        public static void Main()
        {
            Configuration.Default.BasePath = "http://localhost";
            // Configure HTTP basic authorization: basicAuth
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";

            var apiInstance = new AccountsApi(Configuration.Default);
            var accountId = accountId_example;  // string | The id of the account
            var body = ;  // RechargeAccountRequest |  (optional) 

            try
            {
                // Withdraw money from the account
                Account result = apiInstance.WithdrawMoney(accountId, body);
                Debug.WriteLine(result);
            }
            catch (ApiException e)
            {
                Debug.Print("Exception when calling AccountsApi.WithdrawMoney: " + e.Message );
                Debug.Print("Status Code: "+ e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accountId** | **string**| The id of the account | 
 **body** | **RechargeAccountRequest**|  | [optional] 

### Return type

[**Account**](Account.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Expected response to a valid request |  -  |
| **0** | unexpected error |  -  |

[[Back to top]](#)
[[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

