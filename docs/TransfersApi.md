# Io.Gitlab.Mobwa.Api.TransfersApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CompleteTransfer**](TransfersApi.md#completetransfer) | **POST** /accounts/{accountId}/transfers/{transferId}/complete | Complete a transfer
[**CreateTransfer**](TransfersApi.md#createtransfer) | **POST** /accounts/{accountId}/transfers | Create a transfer
[**DeleteTransfer**](TransfersApi.md#deletetransfer) | **DELETE** /accounts/{accountId}/transfers/{transferId} | Delete a transfer
[**GetTransfer**](TransfersApi.md#gettransfer) | **GET** /accounts/{accountId}/transfers/{transferId} | Retrieve information for a specific transfer
[**ListTransfers**](TransfersApi.md#listtransfers) | **GET** /accounts/{accountId}/transfers | List all transfers related to the account
[**UpdateTransfer**](TransfersApi.md#updatetransfer) | **PUT** /accounts/{accountId}/transfers/{transferId} | Change transfer information



## CompleteTransfer

> Transfer CompleteTransfer (string accountId, string transferId)

Complete a transfer

### Example

```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Io.Gitlab.Mobwa.Api;
using Io.Gitlab.Mobwa.Client;
using Io.Gitlab.Mobwa.Model;

namespace Example
{
    public class CompleteTransferExample
    {
        public static void Main()
        {
            Configuration.Default.BasePath = "http://localhost";
            // Configure HTTP basic authorization: basicAuth
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";

            var apiInstance = new TransfersApi(Configuration.Default);
            var accountId = accountId_example;  // string | The id of the account
            var transferId = transferId_example;  // string | The id of the transfer to operate on

            try
            {
                // Complete a transfer
                Transfer result = apiInstance.CompleteTransfer(accountId, transferId);
                Debug.WriteLine(result);
            }
            catch (ApiException e)
            {
                Debug.Print("Exception when calling TransfersApi.CompleteTransfer: " + e.Message );
                Debug.Print("Status Code: "+ e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accountId** | **string**| The id of the account | 
 **transferId** | **string**| The id of the transfer to operate on | 

### Return type

[**Transfer**](Transfer.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Expected response to a valid request |  -  |
| **0** | unexpected error |  -  |

[[Back to top]](#)
[[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## CreateTransfer

> Transfer CreateTransfer (string accountId, CreateTransferRequest createTransferRequest = null)

Create a transfer

### Example

```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Io.Gitlab.Mobwa.Api;
using Io.Gitlab.Mobwa.Client;
using Io.Gitlab.Mobwa.Model;

namespace Example
{
    public class CreateTransferExample
    {
        public static void Main()
        {
            Configuration.Default.BasePath = "http://localhost";
            // Configure HTTP basic authorization: basicAuth
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";

            var apiInstance = new TransfersApi(Configuration.Default);
            var accountId = accountId_example;  // string | The id of the account
            var createTransferRequest = new CreateTransferRequest(); // CreateTransferRequest |  (optional) 

            try
            {
                // Create a transfer
                Transfer result = apiInstance.CreateTransfer(accountId, createTransferRequest);
                Debug.WriteLine(result);
            }
            catch (ApiException e)
            {
                Debug.Print("Exception when calling TransfersApi.CreateTransfer: " + e.Message );
                Debug.Print("Status Code: "+ e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accountId** | **string**| The id of the account | 
 **createTransferRequest** | [**CreateTransferRequest**](CreateTransferRequest.md)|  | [optional] 

### Return type

[**Transfer**](Transfer.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **201** | Transfer successfully created |  -  |
| **0** | unexpected error |  -  |

[[Back to top]](#)
[[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DeleteTransfer

> Transfer DeleteTransfer (string accountId, string transferId)

Delete a transfer

### Example

```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Io.Gitlab.Mobwa.Api;
using Io.Gitlab.Mobwa.Client;
using Io.Gitlab.Mobwa.Model;

namespace Example
{
    public class DeleteTransferExample
    {
        public static void Main()
        {
            Configuration.Default.BasePath = "http://localhost";
            // Configure HTTP basic authorization: basicAuth
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";

            var apiInstance = new TransfersApi(Configuration.Default);
            var accountId = accountId_example;  // string | The id of the account
            var transferId = transferId_example;  // string | The id of the transfers to operate on

            try
            {
                // Delete a transfer
                Transfer result = apiInstance.DeleteTransfer(accountId, transferId);
                Debug.WriteLine(result);
            }
            catch (ApiException e)
            {
                Debug.Print("Exception when calling TransfersApi.DeleteTransfer: " + e.Message );
                Debug.Print("Status Code: "+ e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accountId** | **string**| The id of the account | 
 **transferId** | **string**| The id of the transfers to operate on | 

### Return type

[**Transfer**](Transfer.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Null response |  -  |
| **0** | unexpected error |  -  |

[[Back to top]](#)
[[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetTransfer

> Transfer GetTransfer (string accountId, string transferId)

Retrieve information for a specific transfer

### Example

```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Io.Gitlab.Mobwa.Api;
using Io.Gitlab.Mobwa.Client;
using Io.Gitlab.Mobwa.Model;

namespace Example
{
    public class GetTransferExample
    {
        public static void Main()
        {
            Configuration.Default.BasePath = "http://localhost";
            // Configure HTTP basic authorization: basicAuth
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";

            var apiInstance = new TransfersApi(Configuration.Default);
            var accountId = accountId_example;  // string | The id of the account
            var transferId = transferId_example;  // string | The id of the transfers to operate on

            try
            {
                // Retrieve information for a specific transfer
                Transfer result = apiInstance.GetTransfer(accountId, transferId);
                Debug.WriteLine(result);
            }
            catch (ApiException e)
            {
                Debug.Print("Exception when calling TransfersApi.GetTransfer: " + e.Message );
                Debug.Print("Status Code: "+ e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accountId** | **string**| The id of the account | 
 **transferId** | **string**| The id of the transfers to operate on | 

### Return type

[**Transfer**](Transfer.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Expected response to a valid request |  -  |
| **0** | unexpected error |  -  |

[[Back to top]](#)
[[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ListTransfers

> List&lt;Transfer&gt; ListTransfers (string accountId)

List all transfers related to the account

### Example

```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Io.Gitlab.Mobwa.Api;
using Io.Gitlab.Mobwa.Client;
using Io.Gitlab.Mobwa.Model;

namespace Example
{
    public class ListTransfersExample
    {
        public static void Main()
        {
            Configuration.Default.BasePath = "http://localhost";
            // Configure HTTP basic authorization: basicAuth
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";

            var apiInstance = new TransfersApi(Configuration.Default);
            var accountId = accountId_example;  // string | The id of the account

            try
            {
                // List all transfers related to the account
                List<Transfer> result = apiInstance.ListTransfers(accountId);
                Debug.WriteLine(result);
            }
            catch (ApiException e)
            {
                Debug.Print("Exception when calling TransfersApi.ListTransfers: " + e.Message );
                Debug.Print("Status Code: "+ e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accountId** | **string**| The id of the account | 

### Return type

[**List&lt;Transfer&gt;**](Transfer.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | A list of transfers related to the the account |  -  |
| **0** | unexpected error |  -  |

[[Back to top]](#)
[[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UpdateTransfer

> Transfer UpdateTransfer (string accountId, string transferId, UpdateTransferRequest updateTransferRequest = null)

Change transfer information

### Example

```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Io.Gitlab.Mobwa.Api;
using Io.Gitlab.Mobwa.Client;
using Io.Gitlab.Mobwa.Model;

namespace Example
{
    public class UpdateTransferExample
    {
        public static void Main()
        {
            Configuration.Default.BasePath = "http://localhost";
            // Configure HTTP basic authorization: basicAuth
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";

            var apiInstance = new TransfersApi(Configuration.Default);
            var accountId = accountId_example;  // string | The id of the account
            var transferId = transferId_example;  // string | The id of the transfers to operate on
            var updateTransferRequest = new UpdateTransferRequest(); // UpdateTransferRequest |  (optional) 

            try
            {
                // Change transfer information
                Transfer result = apiInstance.UpdateTransfer(accountId, transferId, updateTransferRequest);
                Debug.WriteLine(result);
            }
            catch (ApiException e)
            {
                Debug.Print("Exception when calling TransfersApi.UpdateTransfer: " + e.Message );
                Debug.Print("Status Code: "+ e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accountId** | **string**| The id of the account | 
 **transferId** | **string**| The id of the transfers to operate on | 
 **updateTransferRequest** | [**UpdateTransferRequest**](UpdateTransferRequest.md)|  | [optional] 

### Return type

[**Transfer**](Transfer.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **201** | Null response |  -  |
| **0** | unexpected error |  -  |

[[Back to top]](#)
[[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

