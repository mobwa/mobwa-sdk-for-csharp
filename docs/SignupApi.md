# Io.Gitlab.Mobwa.Api.SignupApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**Signup**](SignupApi.md#signup) | **POST** /signup | Signup



## Signup

> SignUpResponse Signup (SignUpRequest signUpRequest = null)

Signup

### Example

```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Io.Gitlab.Mobwa.Api;
using Io.Gitlab.Mobwa.Client;
using Io.Gitlab.Mobwa.Model;

namespace Example
{
    public class SignupExample
    {
        public static void Main()
        {
            Configuration.Default.BasePath = "http://localhost";
            var apiInstance = new SignupApi(Configuration.Default);
            var signUpRequest = new SignUpRequest(); // SignUpRequest |  (optional) 

            try
            {
                // Signup
                SignUpResponse result = apiInstance.Signup(signUpRequest);
                Debug.WriteLine(result);
            }
            catch (ApiException e)
            {
                Debug.Print("Exception when calling SignupApi.Signup: " + e.Message );
                Debug.Print("Status Code: "+ e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **signUpRequest** | [**SignUpRequest**](SignUpRequest.md)|  | [optional] 

### Return type

[**SignUpResponse**](SignUpResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Information related to the enrolled user |  -  |
| **0** | unexpected error |  -  |

[[Back to top]](#)
[[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

