
# Io.Gitlab.Mobwa.Model.Account

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** |  | [optional] 
**Name** | **string** |  | [optional] 
**CreatedAt** | **string** |  | [optional] 
**Currency** | **string** |  | [optional] 
**UpdatedAt** | **string** |  | [optional] 
**Balance** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models)
[[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to README]](../README.md)

