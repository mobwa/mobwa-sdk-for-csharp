
# Io.Gitlab.Mobwa.Model.CreateTransferRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Message** | **string** |  | [optional] 
**Amount** | **int** |  | [optional] 
**Currency** | **string** |  | [optional] 
**AutoComplete** | **bool** |  | [optional] 
**Destination** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models)
[[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to README]](../README.md)

