
# Io.Gitlab.Mobwa.Model.Transfer

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** |  | [optional] 
**Message** | **string** |  | [optional] 
**Amount** | **int** |  | [optional] 
**Currency** | **string** |  | [optional] 
**CreatedAt** | **string** |  | [optional] 
**UpdatedAt** | **string** |  | [optional] 
**Status** | **string** |  | [optional] 
**AutoComplete** | **bool** |  | [optional] 
**Source** | [**TransferSource**](TransferSource.md) |  | [optional] 
**Destination** | [**TransferSource**](TransferSource.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models)
[[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to README]](../README.md)

